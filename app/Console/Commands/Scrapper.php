<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\DomCrawler\Crawler;

class Scrapper extends Command
{

    private $client;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scrape:start';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scrape data from specific URL';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->client = new \GuzzleHttp\Client([
            'timeout'   => 10,
            'verify'    => false
        ]);
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            $response = $this->client->get('https://www.falconliving.com/featured/'); // URL, where you want to fetch the content
            // get content and pass to the crawler
            $content = $response->getBody()->getContents();
            $crawler = new Crawler( $content );

            $_this = $this;
            $data = $crawler->filter('div.property')
                ->each(function (Crawler $node, $i) use($_this) {
                    return $_this->getNodeContent($node);
                }
                );

            Storage::disk('local')->put(time().'.json',json_encode($data));
            echo 'Your Data Scrapped Successfully.';

        } catch ( \Exception $e ) {
//            echo $e->getMessage();
            echo 'Something Went Wrong. --'.$e->getMessage();
        }
    }

    /**
     * Get node values
     * @filter function required the identifires, which we want to filter from the content.
     */
    private function getNodeContent($node)
    {
        $array = [
            'address' => $node->filter('.property-detail-section .address') != false ? $node->filter('.property-detail-section .address')->text() : '',
            'price' => $node->filter('.property-detail-section .price') != false ? $node->filter('.property-detail-section .price')->text() : '',
            'description' => $node->filter('.property-detail-section .featured-property-description') != false ? $node->filter('.property-detail-section .featured-property-description')->text() : '',
            'detail' => $node->filter('.property-detail-section .detail')->each(function ($detailNode){
                return $detailNode->text();
            }),
            'featured_image' => $node->filter('.property-thumb a img') != false ? $node->filter('.property-thumb a img')->eq(0)->attr('src') : ''
        ];

        return $array;
    }
}
