<?php

namespace App\Helpers;

use Illuminate\Support\Facades\File;

/**
 * CSVHelper
 */
class CSVHelper
{
    /**
     *
     */
    const DS = DIRECTORY_SEPARATOR;
    /**
     * @param string $filename
     * @param string $delimiter
     * @return array|bool
     */
    public static function csvToArray(string $filename = '', string $delimiter = ',') {
        if (!file_exists($filename) || !is_readable($filename))
            return false;

        $header = null;
        $data = array();
        if (($handle = fopen($filename, 'r')) !== false) {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false) {
                if (!$header)
                    $header = $row;
                else
                    $data[] = array_combine($header, $row);
            }
            fclose($handle);
        }
        return $data;
    }

    /**
     * @param string $filename
     * @param array $csvHeader
     * @param array $csvData
     */
    public static function createCSVFile(string $filename, array $csvHeader, array $csvData)
    {
        $handle = fopen($filename, 'w+');

        fputcsv($handle, $csvHeader);

        foreach ($csvData as $data) {
            fputcsv($handle, $data);
        }

        fclose($handle);
    }

    /**
     * @return string
     */
    public static function getSampleFilePath(): string
    {
        $path = public_path().self::DS.'sample';
        File::ensureDirectoryExists($path, 0777, true);
        return $path;
    }
}
