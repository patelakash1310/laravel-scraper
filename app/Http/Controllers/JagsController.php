<?php

namespace App\Http\Controllers;

//use Goutte\Client;
use App\Helpers\CSVHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Sunra\PhpSimple\HtmlDomParser;
use Symfony\Component\DomCrawler\Crawler;

class JagsController extends Controller
{

    private $client;
    /**
     * Class __contruct
     */
    public function __construct()
    {
        $this->client = new \GuzzleHttp\Client([
            'timeout'   => 10,
            'verify'    => false
        ]);
    }
    /**
     * Content Crawler
     */
    public function getCrawlerContent()
    {
        try {

            $url = 'https://chefset.co.uk/collections/uniform?view=48';

             $response = $this->client->get($url); // URL, where you want to fetch the content
            // get content and pass to the crawler
            $content = $response->getBody()->getContents();
            $crawler = new Crawler( $content );

            $_this = $this;
            $data = $crawler->filter('div.product')
                ->each(function (Crawler $node, $i) use($_this) {
                    return $_this->getNodeContent($node);
                });

//            $innerdata = [];
//            foreach ($data as $innerlink)
//            {
//
//                $response = $this->client->get($innerlink['link']); // URL, where you want to fetch the content
//
//                // get content and pass to the crawler
//                $content = $response->getBody()->getContents();
//                sleep(4);
//                $crawler = new Crawler( $content );
//
//
//                dd($crawler->filter('div.product-details')->html());
//                $_this = $this;
//                $inndata['name'] = $crawler->filter('div.product-details h1.product-header')->text();
//                $inndata['sku'] = $crawler->filter('span.variant-sku')->text() != false ? 'ss' : 'ff';
//                $inndata['image'] = $crawler->filter('div.product-images a img') != false ? 'https:'. str_replace('120x','1020x',substr($crawler->filter('.product-images img')->eq(0)->attr('src'), 0, strpos($crawler->filter('.product-images img')->eq(0)->attr('src'), "?v=")))  : '';
//
//                dd($inndata);
//            }
            $storage = Storage::disk('local');
            $ds = DIRECTORY_SEPARATOR;

            $filePath = CSVHelper::getSampleFilePath().CSVHelper::DS.'chefset.csv';
            $csvHeader = ['Product Name', 'SKU','Image'];
            $this->createCSVFile($filePath, $csvHeader, $data);


            Storage::put(time().'.json',json_encode($data));
//            dd(json_encode($data));
        } catch ( \Exception $e ) {
            echo $e->getMessage();
        }
    }

    /**
     * Get node values
     * @filter function required the identifires, which we want to filter from the content.
     */
    private function getNodeContent($node)
    {
        $array = [
//            'address' => $node->filter('.property-detail-section .address') != false ? $node->filter('.property-detail-section .address')->text() : '',
//            'price' => $node->filter('.product-price h6') != false ? $node->filter('.product-price h6')->text() : '',
//            'description' => $node->filter('.property-detail-section .featured-property-description') != false ? $node->filter('.property-detail-section .featured-property-description')->text() : '',
            'name' => $node->filter('.product-title h5 a') != false ? $node->filter('.product-title h5 a')->text()  : '',
            'sku' => $node->filter('.product-title h5 a') != false ? str_replace(')','',substr($node->filter('.product-title h5 a')->text(),'-5'))  : '',
//            'link' => $node->filter('.product-thumb a') != false ? 'https://chefset.co.uk'.$node->filter('.product-thumb  a')->eq(0)->attr('href') : '',
//
//            'detail' => $node->filter('.property-detail-section .detail')->each(function ($detailNode){
//                return $detailNode->text();
//            }),
           'featured_image' => $node->filter('.product-thumb a img') != false ? 'https:'. str_replace('120x','1020x',substr($node->filter('.product-thumb a img')->eq(0)->attr('src'), 0, strpos($node->filter('.product-thumb a img')->eq(0)->attr('src'), "?v=")))  : ''
        ];

        return $array;
    }

    public static function createCSVFile(string $filename, array $csvHeader, array $csvData)
    {
        $handle = fopen($filename, 'w+');

        fputcsv($handle, $csvHeader);

        foreach ($csvData as $data) {
            fputcsv($handle, $data);
        }

        fclose($handle);
    }
}
