<?php

namespace App\Http\Controllers;

//use Goutte\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Sunra\PhpSimple\HtmlDomParser;
use Symfony\Component\DomCrawler\Crawler;

class ScrapeController extends Controller
{

    private $client;
    /**
     * Class __contruct
     */
    public function __construct()
    {
        $this->client = new \GuzzleHttp\Client([
            'timeout'   => 10,
            'verify'    => false
        ]);
    }
    /**
     * Content Crawler
     */
    public function getCrawlerContent()
    {
        try {

            $response = $this->client->get('https://www.falconliving.com/featured/'); // URL, where you want to fetch the content
//            $response = $this->client->get('https://www.thebottleclub.com/products/ciroc-summer-watermelon-vodka'); // URL, where you want to fetch the content
//            $response = $this->client->get('https://chefset.co.uk/collections/bakeware'); // URL, where you want to fetch the content
            // get content and pass to the crawler
            $content = $response->getBody()->getContents();
            $crawler = new Crawler( $content );

            $_this = $this;
            $data = $crawler->filter('div.property')
                ->each(function (Crawler $node, $i) use($_this) {
                    return $_this->getNodeContent($node);
                });

//        $data = $crawler->filter('div.product-single .product-single__title')->text();



            dd($data);
            Storage::put(time().'.json',json_encode($data));
//            dd(json_encode($data));
        } catch ( \Exception $e ) {
            echo $e->getMessage();
        }
    }

    /**
     * Get node values
     * @filter function required the identifires, which we want to filter from the content.
     */
    private function getNodeContent($node)
    {
        $array = [
            'address' => $node->filter('.property-detail-section .address') != false ? $node->filter('.property-detail-section .address')->text() : '',
            'price' => $node->filter('.property-detail-section .price') != false ? $node->filter('.property-detail-section .price')->text() : '',
            'description' => $node->filter('.property-detail-section .featured-property-description') != false ? $node->filter('.property-detail-section .featured-property-description')->text() : '',
            'detail' => $node->filter('.property-detail-section .detail') != false ?  : '',

            'detail' => $node->filter('.property-detail-section .detail')->each(function ($detailNode){
                return $detailNode->text();
            }),
           'featured_image' => $node->filter('.property-thumb a img') != false ? $node->filter('.property-thumb a img')->eq(0)->attr('src') : ''
        ];

        return $array;
    }
}
